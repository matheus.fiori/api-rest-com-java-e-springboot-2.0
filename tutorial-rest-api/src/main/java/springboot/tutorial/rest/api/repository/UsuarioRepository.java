package springboot.tutorial.rest.api.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import springboot.tutorial.rest.api.model.UsuarioModel;

import java.util.List;

@Repository
public interface UsuarioRepository extends CrudRepository<UsuarioModel, Integer> {
    UsuarioModel findByNome(String nome);

    List<UsuarioModel> findByNomeLike(String nome);
}
