package springboot.tutorial.rest.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springboot.tutorial.rest.api.model.UsuarioModel;
import springboot.tutorial.rest.api.repository.UsuarioRepository;

import java.util.List;

@RestController
public class UsuarioController {

    @Autowired
    private UsuarioRepository repository;

    //UsuarioModel usuario = new UsuarioModel();

    @GetMapping(path = "/api/usuario/{codigo}")
    public ResponseEntity consultar(@PathVariable("codigo") Integer codigo){
        return repository.findById(codigo)
                .map(record -> ResponseEntity.ok().body(record))
                .orElse(ResponseEntity.notFound().build());
    }


    @GetMapping(path = "/api/usuario/nome/{nome}")
    public ResponseEntity<UsuarioModel> consultar(@PathVariable("nome") String nome){
        UsuarioModel usuario = repository.findByNome(nome);
        return ResponseEntity.ok().body(usuario);
    }

    @PutMapping(path = "/api/usuario/update/{codigo}")
    public ResponseEntity update(@PathVariable("codigo") Integer codigo,
                                 @RequestBody UsuarioModel usuario){
        return repository.findById(codigo)
                .map(record -> {
                    record.setNome(usuario.getNome());
                    record.setLogin(usuario.getLogin());
                    record.setSenha(usuario.getSenha());
                    UsuarioModel updated = repository.save(record);
                    return ResponseEntity.ok() .body(updated);
                }).orElse(ResponseEntity.notFound().build());
    }

    @GetMapping(path = "/api/usuario/nome")
    public ResponseEntity<List<UsuarioModel>> getUsuarioByNome(@RequestParam String nome){
        return new ResponseEntity<List<UsuarioModel>>(repository.findByNomeLike("%"+nome+"%"), HttpStatus.OK);
    }

    @PostMapping(path = "/api/usuario/salvar")
    public UsuarioModel salvar(@RequestBody UsuarioModel usuario) {
        return repository.save(usuario);
    }

}
